import React, {useState} from 'react';
import {Button, Stack, Typography} from "@mui/material";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import PropTypes from 'prop-types';


export default function ReadMoreText ({value, alignmentText = "left", textSize = 13, textColor = "black"}) {

    /**
     * alignmentText = 'left/right/center'
     * textSize = 20
     * textColor = "white"/"#fefefe"
     */

    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('sm'));
    /**
     * Show More Code
     */
    const [showMore, setShowMore] = useState(false);
    const handleShowHideText = () => {
        setShowMore(!showMore);
    }

    return(

        <>
            {
                value.split('<br />').map((each, pos) => {
                    if(pos === 0){
                        return(
                            <Typography align={alignmentText} sx={{
                                mt: 1,
                                fontSize: textSize,
                                lineHeight: 1.1,
                                color: textColor,
                            }}>
                                {each}
                                {
                                    (!matches && !showMore) && <span style={{color: 'green', fontWeight: '600'}} onClick={handleShowHideText}>
                                        More...
                                    </span>
                                }

                            </Typography>
                        );
                    }else {
                        if(showMore){
                            return(
                                <Typography align={alignmentText} sx={{
                                    mt: 1,
                                    fontSize: textSize,
                                    lineHeight: 1.1,
                                    color: textColor,
                                }}>
                                    {each}
                                    {
                                        (!matches && showMore && pos === (value.split('<br />').length - 1)) &&
                                        <span style={{color: 'green', fontWeight: '600'}} onClick={handleShowHideText}>
                                                Less...
                                            </span>
                                    }
                                </Typography>
                            );
                        }else {
                            return(<></>);
                        }
                    }
                })
            }
            {
                matches && <Stack justifyContent={'center'} alignItems={'center'} width={'100%'} mt={2}>
                    <Button variant={'outlined'} onClick={handleShowHideText}>
                        {`Show ${showMore ? 'Less' : 'More'}`}
                    </Button>
                </Stack>
            }
        </>
    )
}
ReadMoreText.propTypes = {
    value: PropTypes.string.isRequired,
    textSize: PropTypes.number,
    alignmentText: PropTypes.oneOf(['left', 'right', 'center']),
    textColor: PropTypes.string,

}