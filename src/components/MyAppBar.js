import * as React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import {useEffect, useState} from "react";
import {Hidden, Stack} from "@mui/material";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

const drawerWidth = 240;
const navItems = ['Home', 'About', 'Contact'];

function DrawerAppBar(props) {
    const theme = useTheme();
    const { window, children } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [user, setUser] = useState();
    const narrowMobile = useMediaQuery('(max-width: 300px)');
    const tablet = useMediaQuery(theme.breakpoints.only('sm'));
    const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    const desktop = useMediaQuery(theme.breakpoints.up('md'));
    console.log('narrowMobile --> ',narrowMobile)
    useEffect(() => {
        setUser(
            JSON.parse(localStorage.getItem('test-user'))
        )
    }, [window]);


    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Typography variant="h6" sx={{ my: 2 }}>
                MUI
            </Typography>
            <Divider />
            <List>
                {navItems.map((item) => (
                    <ListItem key={item} disablePadding>
                        <ListItemButton sx={{ textAlign: 'center' }}>
                            <ListItemText primary={item} />
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <Stack alignItems={'center'}>
            <CssBaseline />
            <Stack sx={{
                position: 'fixed',
                top: 0,
                left: 0,
                alignItems: 'center',
                height: {xs: 90, sm: 90, md: 110},
                width: '100%'
            }}>
                {
                    (mobile && !narrowMobile) &&
                    <img height={'auto'} width={'96%'} src={'/images/appbar/mobile-bg.svg'} alt={'app-bar-bg'} />
                }
                {
                    (mobile && narrowMobile) &&
                    <img height={'auto'} width={'96%'} src={'/images/appbar/narrow-bg.svg'} alt={'app-bar-bg'} />
                }
                {
                    tablet &&
                    <img height={'auto'} width={'96%'} src={'/images/appbar/tablet-bg.svg'} alt={'app-bar-bg'} />
                }
                {
                    desktop &&
                    <img height={'auto'} width={'96%'} src={'/images/appbar/appbar-bg.svg'} alt={'app-bar-bg'} />
                }

            </Stack>
            <Stack sx={{
                height: {xs: 90, sm: 90, md: 110},
                width: '90%',
                pt: 4,
                zIndex: 10,
            }}>
                <Box sx={{
                    display: 'flex'
                }}>
                    {navItems.map((item) => (
                        <Typography sx={{
                            color: 'white',
                            mr: 2,
                        }}>
                            {item}
                        </Typography>
                    ))}
                </Box>
            </Stack>



            <Box component="nav">
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'none' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box component="main">
                {children}
            </Box>
        </Stack>
    );
}

DrawerAppBar.propTypes = {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};

export default DrawerAppBar;
