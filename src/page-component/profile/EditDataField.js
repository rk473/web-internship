import React from 'react';
import {Box, Stack, TextField, Typography} from "@mui/material";


export default function EditDataField ({label, value, onChange, edit, helperText}) {

    return(
        <Stack px={2} pb={1}>
            {
                edit ?
                    <>
                        <Typography variant={'caption'} sx={{mb: 1}}>
                            {label}
                        </Typography>
                        <TextField
                            variant={'outlined'}
                            fullWidth
                            error={!!helperText}
                            helperText={helperText}
                            placeholder={label}
                            value={value}
                            onChange={onChange}
                        />
                    </> :
                    <Box
                        sx={{
                            display: 'flex',
                            alignItems: 'center'
                        }}
                    >
                        <Typography color={'textSecondary'}>
                            {label} :
                        </Typography>
                        <Typography sx={{ml: 2}}>
                            {value}
                        </Typography>
                    </Box>
            }
        </Stack>
    )

}