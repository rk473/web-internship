import {Box, Divider, MenuItem, Paper, Select, Stack, SwipeableDrawer, Typography} from "@mui/material";
import {useEffect, useState} from "react";

import InputBase from '@mui/material/InputBase';
import { styled } from '@mui/material/styles';
import {KeyboardArrowDown, LocationOn} from "@mui/icons-material";
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Button from "@mui/material/Button";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

const AntTabs = styled(Tabs)({
    '& .MuiTabs-indicator': {
        backgroundColor: '#00000000',
    },
});

const AntTab = styled((props) => <Tab disableRipple {...props} />)(({ theme }) => ({
    textTransform: 'capitalize',
    minWidth: 100,
    '&:hover': {
        backgroundColor: '#fff',
        opacity: 1,
    },
    '&.Mui-selected': {
        color: '#000',
        fontWeight: '700',
        backgroundColor: '#fff',
    },
    '&.Mui-focusVisible': {
        backgroundColor: '#fff',
    },
}));
const LINES = 2;
export default function Home() {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));
    /**
     * Input -> InputBase -> FormControl [OutlinedInput , FilledInput] -> TextField
     *
     */
    const [numbers, setNumbers] = useState([]);
    const [value, setValue] = useState(0);
    const [open, setOpen] = useState(false);

    const handleChangeTabs = (event, newValue) => {
        setValue(newValue);
    };

    useEffect(() => {
        const arr = ['1','2','3'];
        console.log('Arr---> ', arr);
        const arr1 = [1,2,3,4,5,6];
        console.log('Arr 1 ---> ', arr1);
        const arr2 = [
            {title: 'One', value: 1},
            {title: 'Two', value: 2},
            {title: 'Three', value: 3},
            {value: 5},
            {title: 'Four', value: 4},
        ];
        console.log('Arr 2 ---> ', arr2);
        const arr3 = new Array(10).fill(1);
        console.log('Arr 3 ---> ', arr3);
        const arr4 = Array.from('rajeskumarsahoo');
        console.log('Arr 4 ---> ', arr4);
        const arrayLike = {0:1, 1:2, 2:3, length: 6};
        const arr5 = Array.from(arrayLike);
        console.log('Arr 5 ---> ', arr5);
        const arr6 = getArrayFromRange(10, 100); //10, 12, 14, 16, 18, 20
        console.log('Arr 6 ---> ', arr6);
        setNumbers(arr6);
    }, []);
    const getArrayFromRange = (start, end, gap = 1) => { // 0 1 2 3 4 5 6
        const arr = Array.from({length: ((end - start)/gap) + 1}, (value, index) => (index * gap) + start);
        return arr;
    }


    const [age, setAge] = useState(10);
    const handleChange = (event) => {
        setAge(event.target.value);
    };

    // 1 2 3 4 5 6 7 8 9 10 11 11/2 = 5 + 1
    // 1 3 5 7 9  11
    // 2 4 6 8 10 x
    return (
        <>
            <Box display={'flex'} justifyContent={'center'} alignItems={'center'} width={'100vw'}>
                <Box display={'flex'} sx={{overflow: 'scroll', width: '95%', mb: 3}}>
                    {
                        numbers && numbers.length > 0 && (new Array(parseInt(numbers.length/LINES) + 1)).fill(1).map((each, pos) => {
                            return(
                                <Box>
                                    {
                                        pos*LINES < numbers.length && numbers[pos*LINES] &&
                                        <Box
                                            sx={{
                                                mr: 2,
                                                mb: 1
                                            }}
                                        >
                                            <img src={'/images/demo.png'} height={250} width={250} />
                                            {/*<Typography width={'200px'}>*/}
                                            {/*    Each - {each.toString()} - {typeof each}*/}
                                            {/*    <br />*/}
                                            {/*    Position - {pos}*/}
                                            {/*</Typography>*/}
                                            {/*<Typography width={'200px'}>*/}
                                            {/*    Each - {each.title || 'No title'} - {typeof each.title}*/}
                                            {/*    <br />*/}
                                            {/*    Each - {each.value || 'No Value'} - {typeof each.value}*/}
                                            {/*    <br />*/}
                                            {/*    Position - {pos}*/}
                                            {/*</Typography>*/}
                                        </Box>
                                    }
                                    {
                                        ((pos*LINES) + 1) < numbers.length && numbers[(pos*LINES) + 1] &&
                                        <Box
                                            sx={{
                                                mr: 2
                                            }}
                                        >
                                            <img src={'/images/demo.png'} height={250} width={250} />
                                            {/*<Typography width={'200px'}>*/}
                                            {/*    Each - {each.toString()} - {typeof each}*/}
                                            {/*    <br />*/}
                                            {/*    Position - {pos}*/}
                                            {/*</Typography>*/}
                                            {/*<Typography width={'200px'}>*/}
                                            {/*    Each - {each.title || 'No title'} - {typeof each.title}*/}
                                            {/*    <br />*/}
                                            {/*    Each - {each.value || 'No Value'} - {typeof each.value}*/}
                                            {/*    <br />*/}
                                            {/*    Position - {pos}*/}
                                            {/*</Typography>*/}
                                        </Box>
                                    }
                                </Box>
                            )
                        })
                    }
                </Box>
            </Box>
            <Stack width={'100vw'} height={600} bgcolor={'green'} p={4}>

                <Box display={'flex'} alignItems={'center'} width={'50%'} height={50} bgcolor={'white'} borderRadius={1.2} mb={4}>
                    <LocationOn sx={{ml: 1.4, color: 'green'}}/>
                    <Select
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={age}
                        sx={{width: 150, ml: 1.2}}
                        IconComponent={KeyboardArrowDown}
                        onChange={handleChange}
                        input={<InputBase />}
                    >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                    <Divider orientation={'vertical'} sx={{height: '80%'}}/>
                    <InputBase
                        placeholder={'Type Something'}
                        sx={{width: '100%', ml: 2.2}}
                    />
                </Box>

                <Box bgcolor={'#E8E8E8'} borderRadius={2} p={2} display={'flex'} alignItems={'center'} width={300}>
                    <Box width={65} height={65} bgcolor={'white'}>
                    </Box>
                    <Box ml={2}>
                        <Typography sx={{fontSize: 18}}>
                            Utkal Builders
                        </Typography>
                        <Typography sx={{fontSize: 16}} color={'textSecondary'}>
                            Bhubaneswar, Odisha
                        </Typography>
                    </Box>
                </Box>
                <Box sx={{ bgcolor: '#F2F3F8', px: 2, pt: 2 }}>
                    <AntTabs value={value} onChange={handleChangeTabs} aria-label="ant example">
                        <AntTab label="Tab 1" />
                        <AntTab label="Tab 2" />
                        <AntTab label="Tab 3" />
                    </AntTabs>
                    <Box sx={{ p: 3, bgcolor: 'white' }} />
                </Box>
                <Box>
                    <Button onClick={() => setOpen(true)}>
                        Open Drawer
                    </Button>
                    <SwipeableDrawer
                        anchor={matches ? 'right' : 'left'}
                        open={open}
                        onClose={() => setOpen(false)}
                        onOpen={() => setOpen(true)}
                    >
                        <Typography>
                            asdasd sad ad
                        </Typography>
                    </SwipeableDrawer>
                </Box>
            </Stack>
        </>
    )
}
