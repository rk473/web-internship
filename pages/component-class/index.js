import React from 'react';
import {Box, Grid, Stack,} from "@mui/material";
import ReadMoreText from "../../src/components/ReadMoreText";


const bigString = ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et lorem quis leo porttitor varius. Duis aliquet elit nunc, vel posuere nulla euismod nec. Proin vitae viverra nunc, in porta neque. Duis interdum, elit id finibus vulputate, libero mi commodo tortor, vehicula cursus risus lacus varius ipsum. Nam id lacus eu sem porta fringilla. Suspendisse id diam enim. Suspendisse scelerisque, risus in ornare fringilla, est nibh condimentum velit, at semper nisl ex vitae diam. Pellentesque a hendrerit ipsum. Vivamus sed tempor nisi, nec porttitor nunc. Cras mollis tortor vel pharetra euismod.<br />\n' +
    '                    Donec id convallis leo. Phasellus ut ornare libero, quis cursus tortor. Ut quis leo quis eros maximus pulvinar. Sed eu magna sem. Pellentesque in tellus id nunc posuere fringilla id et ante. Fusce et ex suscipit, aliquet dui eget, pellentesque dolor. Maecenas venenatis purus sed nibh tincidunt, sed faucibus purus semper. Proin non dictum risus. Curabitur tincidunt tellus lorem, id convallis ipsum auctor at. Sed vel lacus at lorem mollis elementum gravida eu magna. Nullam hendrerit dapibus augue id tincidunt. Donec nec rhoncus leo.<br />\n' +
    '                    Proin nisl sapien, ultricies non pharetra quis, ullamcorper quis leo. Praesent arcu velit, aliquet sit amet odio quis, consequat tincidunt est. In sed lacus vitae velit ultricies tempor. Aenean hendrerit vitae est consectetur dapibus. Praesent consectetur aliquam laoreet. Donec volutpat sed lectus non suscipit. Pellentesque commodo ornare nibh et porttitor. Donec ac quam orci. Etiam euismod ipsum sollicitudin neque dapibus porta. Nulla sed ligula sit amet felis pretium egestas. Donec et nisl faucibus, maximus augue id, auctor urna. Suspendisse potenti. Aliquam molestie diam ac ullamcorper cursus.'

export default function Profile () {


    return(
        <Stack p={4}>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <ReadMoreText textColor={'red'} textSize={16} alignmentText={'center'} value={bigString}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <ReadMoreText value={bigString} />
                </Grid>
            </Grid>
            <Stack width={'100%'}>
                <img src={'/images/astro.jpeg'} width={'100%'} height={'auto'}/>
                <Stack sx={{
                    mt: '-120px',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                    height: 120,
                    width: '100%',
                    bgcolor: 'rgba(254,254,254,0.23)',
                }}>
                    {
                        [1,2,3,4].map((each) => (
                            <Box width={80} height={80} bgcolor={'red'} mr={2} sx={{
                                // transition: 'width 0.3s, height 0.3s',
                                '&:hover': {
                                    transform: 'scale(1.1, 1.1)',
                                    // height: 90,
                                    // width: 90
                                }
                            }}>

                            </Box>
                        ))
                    }

                </Stack>
            </Stack>
        </Stack>
    )

}