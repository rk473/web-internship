import {Box, Button, CircularProgress, InputAdornment, Stack, TextField, Typography} from "@mui/material";
import {useState} from "react";
import {useRouter} from "next/router";
import axios from 'axios';
import {useSnackbar} from "notistack";


export default function Home() {

    const [phone, setPhone] = useState('');
    const [phoneError, setPhoneError] = useState('');
    const [otp, setOtp] = useState('');
    const [otpError, setOtpError] = useState('');
    const [loading, setLoading] = useState(false);
    const [otpSent, setOtpSent] = useState(false);
    const Router = useRouter();
    const { enqueueSnackbar } = useSnackbar();

    const validateFields = () => {
        let valid = true;
        if(phone === '' || phone.length < 10){
            setPhoneError('Please enter a valid phone');
            valid = false;
        }else {
            setPhoneError('');
        }
        return valid;
    }

    const validateOTPField = () => {
        let valid = true;
        if(otp === '' || otp.length < 6){
            setOtpError('Please enter a valid otp');
            valid = false;
        }else {
            setOtpError('');
        }
        return valid;
    }


    const handleLogin = () => {
        /**
         * Validations
         */
        if(validateFields()){
            setLoading(true);
            /**
             * Todo - API call
             */
            axios.post(
                'https://api.test.kemnu.com/v2/authenticate',
                {
                    "platform": 1,
                    "payload": {
                        "phone": phone,
                        "countryCode": "91"
                    },
                    "strategy": "phone"
                })
                .then((response) => {
                    /**
                     * Do post login things here
                     */
                    enqueueSnackbar('OTP sent Successfully', {
                        variant: 'success',
                    });
                    setOtpSent(true);
                })
                .catch((error) => {
                    const data = error?.response?.data
                    enqueueSnackbar(data?.message || 'Something went wrong', {
                        variant: 'error',
                        anchorOrigin: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                })
                .finally(() => {
                    setLoading(false);
                })
        }
    }

    const handleVerifyOtp = () => {
        /**
         * Validations
         */
        if(validateOTPField()){
            setLoading(true);
            /**
             * Todo - API call
             */
            axios.patch(
                'https://api.test.kemnu.com/v2/authenticate',
                {
                    "platform": 1,
                    "payload": {
                        "phone": phone,
                        "countryCode": "91"
                    },
                    "otp": otp,
                    "strategy": "phone",
                    "deviceId": "87841212sfdsr53w4sdfds234dsssdf3erdftsdfrpqws"
                })
                .then((response) => {
                    const {data} = response;
                    /**
                     * Do post login things here
                     */
                    enqueueSnackbar('Login Successful', {
                        variant: 'success',
                    });
                    localStorage.setItem('test-token', data?.accessToken);
                    Router.push('/community');
                })
                .catch((error) => {
                    const data = error?.response?.data
                    enqueueSnackbar(data?.message || 'Something went wrong', {
                        variant: 'error',
                        anchorOrigin: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                })
                .finally(() => {
                    setLoading(false);
                })
        }
    }
    return (
        <Box width={'100%'} bgcolor={'white'}>
            <Stack p={3} justifyContent={'center'} alignItems={'center'} height={'100vh'}>
                <Typography sx={{mb: 3}}>
                    Login Page
                </Typography>
                {
                    otpSent ?
                        <TextField
                            placeholder={'Enter the OTP'}
                            type={'number'}
                            value={otp}
                            error={!!otpError} // '' -> false ! true !! false
                            helperText={otpError}
                            variant="outlined"
                            onChange={(e) => {
                                const _val = e.target.value;
                                if(_val.length <= 6)
                                    setOtp(_val);
                            }}
                            sx={{minWidth: 300}}
                        />
                        :
                        <TextField
                            placeholder={'Enter your phone number'}
                            type={'number'}
                            value={phone}
                            error={!!phoneError} // '' -> false ! true !! false
                            helperText={phoneError}
                            variant="outlined"
                            onChange={(e) => {
                                const _val = e.target.value;
                                console.log('Val --> ', _val);
                                if(_val.length <= 10)
                                    setPhone(_val);
                            }}
                            sx={{minWidth: 300}}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        + 91
                                    </InputAdornment>
                                ),
                            }}
                        />
                }

                <Button
                    variant={'contained'}
                    disabled={loading}
                    color={'primary'}
                    onClick={() => {
                        if(otpSent)
                            handleVerifyOtp()
                        else
                            handleLogin()
                    }}
                    sx={{mt: 2, minWidth: 120}}
                >
                    {loading ? <CircularProgress size={18} /> : otpSent ? 'Verify Otp' :'Send OTP'}
                </Button>

            </Stack>
        </Box>
    )
}
