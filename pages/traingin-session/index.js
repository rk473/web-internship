import {Stack, Box, Grid, Chip, TextField, MenuItem} from "@mui/material";
import { styled } from '@mui/material/styles';

const MyText = styled(TextField)({
    '& .MuiOutlinedInput-root': {
        color: '#fff',
        '& fieldset': {
            borderColor: '#72747e',
        },
        '&:hover fieldset': {
            borderColor: '#a8abbb',
        },
        '&.Mui-focused fieldset': {
            borderColor: '#ffffff',
        },
        '& .MuiSelect-icon': {
            color: 'white'
        }
    },

});
export default function JsonData(){
    return(
        <>
            <Box width={'100%'} bgcolor={'white'} height={1050}>

            </Box>
        </>

    )
}
