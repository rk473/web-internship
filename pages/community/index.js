import {
    Avatar,
    Box,
    Button,
    Card,
    CardHeader,
    CircularProgress,
    Grid,
    Stack,
    TextField,
    Typography
} from "@mui/material";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import axios from 'axios';
import {useSnackbar} from "notistack";


export default function Home() {

    const [data, setData] = useState('');
    const [loading, setLoading] = useState(false);
    const Router = useRouter();
    const { enqueueSnackbar } = useSnackbar();
    console.log('List Data --> ', data);
    const loadCommunityData = () => {

            // setLoading(true);
            axios.get(
                'https://api.test.kemnu.com/v2/available-communities',
            )
                .then((response) => {
                    const {data} = response;
                    setData(data);
                })
                .catch((error) => {
                    const data = error?.response?.data
                    enqueueSnackbar(data?.message || 'Something went wrong', {
                        variant: 'error',
                        anchorOrigin: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                })
                .finally(() => {
                    setLoading(false);
                })

    }

    useEffect(() => {
        loadCommunityData();
    }, []);

    return (
        <Box width={'100%'} bgcolor={'white'} minHeight={'100vh'}>
            <Stack p={3}>
                <Typography variant={'h5'} sx={{mb: 3}}>
                    All Available Community
                </Typography>
                <Grid container spacing={2}>
                    {
                        data && data.length > 0 ?
                            data.map((each, pos) => (
                                <Grid item xs={12} sm={6} md={4}>
                                    <Card sx={{ width: '100%', mb: 2 }}>
                                        <CardHeader
                                            avatar={
                                                <Avatar
                                                    sx={{ bgcolor: 'primary.main' }}
                                                    aria-label="recipe"
                                                    src={each?.avatar || ''}
                                                >
                                                    R
                                                </Avatar>
                                            }
                                            title={each?.name || 'No name'}
                                            subheader={each?.code || 'No Code'}
                                        />
                                    </Card>
                                </Grid>

                            )) :
                            <>
                                No Communities Found
                            </>
                    }
                </Grid>

            </Stack>
        </Box>
    )
}
