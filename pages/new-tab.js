import {Avatar, Button, Grid, IconButton, Stack, TextField, Typography} from "@mui/material";
import {Edit, Menu} from "@mui/icons-material";

export default function NewTab() {
    return (
        <Stack
            height={'100vh'}
            width={'100vw'}
            sx={{
                backgroundImage: "url('/images/astro.jpeg')",
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover'
            }}
        >
            <Stack direction={'row'} spacing={2} justifyContent={'flex-end'} alignItems={'center'} px={2} color={'white'} height={70}>
                <Typography>
                    Gmail
                </Typography>
                <Typography>
                    Text
                </Typography>
                <IconButton>
                    <Menu sx={{color: 'white'}} />
                </IconButton>
                <Avatar>
                    A
                </Avatar>
            </Stack>
            <Stack flex={1} justifyContent={'center'} alignItems={'center'}>
                <Stack justifyContent={'center'} alignItems={'center'} color={'white'} flex={1} width={'40%'}>
                    <img height={'auto'} width={300} src={'/images/logo.png'} alt={'logo'} />
                    <TextField sx={{bgcolor: 'white', borderRadius: 6, mt: 3}} fullWidth size={'small'} />
                    <Grid container alignItems={'center'} justifyContent={'center'} width={'90%'} mt={3}>
                        {
                            (new Array(10).fill(1)).map((eah, pos) => (
                                <Grid item md={2.4} key={pos}>
                                    <Button sx={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flexDirection: 'column',
                                        py: 1
                                    }}
                                            fullWidth
                                    >
                                        <Avatar>
                                            {pos}
                                        </Avatar>
                                        <Typography
                                            sx={{color: 'white'}}
                                            variant={"caption"}
                                        >
                                            {'Title '}{pos}
                                        </Typography>
                                    </Button>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Stack>
            </Stack>
            <Stack direction={'row'} spacing={2} justifyContent={'space-between'} alignItems={'center'} px={2} color={'white'} height={70}>
                <Typography>
                    Text on left
                </Typography>
                <IconButton>
                    <Edit sx={{color: 'white'}} />
                </IconButton>
            </Stack>
        </Stack>
    )
}

NewTab.hide = true;
NewTab.title = 'New Tab';