import {Stack} from "@mui/material";
import {useEffect, useState} from "react";

export default function Task2() {
    const [data, setData] = useState([]);
    // const [fields, setFields] = useState([]);

    useEffect(() => {
        const val = {
            age: '',
            name: 'fafasf',
            gender: '',
            row_id: 1
        }
        let _val = {
            "data": [
                {
                    "row_id": 1,
                    "data": {
                        "age": "26",
                        "name": "Guru ",
                        "gender": "Male"
                    }
                },
                {
                    "row_id": 2,
                    "data": {
                        "age": "23",
                        "name": "Dillip"
                    }
                }
            ],
            "field": [
                {
                    "id": 1,
                    "name": "Name",
                    "meta_name": "name",
                    "field_type": "text"
                },
                {
                    "id": 2,
                    "name": "Age",
                    "meta_name": "age",
                    "field_type": "number"
                },
                {
                    "id": 3,
                    "name": "Gender",
                    "meta_name": "gender",
                    "field_type": "text"
                },
                {
                    "id": 4,
                    "name": "Address",
                    "meta_name": "address",
                    "field_type": "text"
                }
            ]
        }
        let finalData = [];
        const fields = _val?.field || [];
        if(fields.length > 0){
            const rawData = _val?.data || [];
            if(rawData.length > 0){
                rawData.map((each, pos) => {
                    let _eachObject = {};
                    _eachObject.row_id = each?.row_id || '';
                    fields.map((eachInner) => {
                        _eachObject[eachInner?.meta_name] = each?.data[eachInner?.meta_name] || '';
                    })
                    finalData.push(_eachObject);
                })
            }
        }
        console.log('Final Data --> ', finalData);
    })
    return (
        <Stack p={3}>
           Task 2
        </Stack>
    )
}

Task2.title = 'Task 2';
