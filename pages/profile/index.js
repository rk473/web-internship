import React, {useEffect, useState} from 'react';
import {Box, Button, Container, Grid, Stack, Typography} from "@mui/material";
import EditDataField from "../../src/page-component/profile/EditDataField";
import axios from "axios";
import {useRouter} from "next/router";
import {useSnackbar} from "notistack";
import ReadMoreText from "../../src/components/ReadMoreText";

const form = [
    {
        label: 'First Name',
        key: 'firstName',
        required: true,
    },
    {
        label: 'Last Name',
        key: 'lastName',
        required: true,
    },
]
const form1 = [
    {
        label: 'Phone',
        key: 'mobile',
        required: true,
    },
    {
        label: 'Telephone No',
        key: 'contactNo',
        required: true,
    },
]

const bigString = ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et lorem quis leo porttitor varius. Duis aliquet elit nunc, vel posuere nulla euismod nec. Proin vitae viverra nunc, in porta neque. Duis interdum, elit id finibus vulputate, libero mi commodo tortor, vehicula cursus risus lacus varius ipsum. Nam id lacus eu sem porta fringilla. Suspendisse id diam enim. Suspendisse scelerisque, risus in ornare fringilla, est nibh condimentum velit, at semper nisl ex vitae diam. Pellentesque a hendrerit ipsum. Vivamus sed tempor nisi, nec porttitor nunc. Cras mollis tortor vel pharetra euismod.<br />\n' +
    '                    Donec id convallis leo. Phasellus ut ornare libero, quis cursus tortor. Ut quis leo quis eros maximus pulvinar. Sed eu magna sem. Pellentesque in tellus id nunc posuere fringilla id et ante. Fusce et ex suscipit, aliquet dui eget, pellentesque dolor. Maecenas venenatis purus sed nibh tincidunt, sed faucibus purus semper. Proin non dictum risus. Curabitur tincidunt tellus lorem, id convallis ipsum auctor at. Sed vel lacus at lorem mollis elementum gravida eu magna. Nullam hendrerit dapibus augue id tincidunt. Donec nec rhoncus leo.<br />\n' +
    '                    Proin nisl sapien, ultricies non pharetra quis, ullamcorper quis leo. Praesent arcu velit, aliquet sit amet odio quis, consequat tincidunt est. In sed lacus vitae velit ultricies tempor. Aenean hendrerit vitae est consectetur dapibus. Praesent consectetur aliquam laoreet. Donec volutpat sed lectus non suscipit. Pellentesque commodo ornare nibh et porttitor. Donec ac quam orci. Etiam euismod ipsum sollicitudin neque dapibus porta. Nulla sed ligula sit amet felis pretium egestas. Donec et nisl faucibus, maximus augue id, auctor urna. Suspendisse potenti. Aliquam molestie diam ac ullamcorper cursus.'

export default function Profile () {
    const Router = useRouter();
    const { enqueueSnackbar } = useSnackbar();
    const [user, setUser] = useState(); //{firstName: 'dasdsadad', lastName: ''}
    const [userError, setUserError] = useState({}); //{firstName: 'First name cannot be empty"}
    const [edit, setEdit] = useState(false);
    const [loading, setLoading] = useState(false);


    useEffect(() => {
        setUser(
            JSON.parse(localStorage.getItem('test-user'))
        )
    }, []);

    const validateFields = () => {
        let _errObject = {};
        let valid = true;
        /**
         * Emtpy Check
         */
        form.map((each) => {
            // 1st loop  & = + | = -
            //user[each?.key] = user ['firstName] = dasdsadad string value = '' / null / undefined (false) !false -> true
            // x + y x = 4 y = 3
            //-(x + y) = -7
            // -4 + -3 = 7
            // -4 - 3
            // -7
            // if(user[each?.key] && user[each?.key] !== ''){
            //     valid = true;
            // }else {
            //     _errObject[each?.key] = each?.label+' is required';
            //     valid = false;
            // }
            //
            // if( !(user[each?.key] && user[each?.key] !== '')){
            //     _errObject[each?.key] = each?.label+' is required';
            //     valid = false;
            // }else {
            //     valid = true;
            // }
            //
            // if( !user[each?.key] || user[each?.key] === ''){
            //     _errObject[each?.key] = each?.label+' is required';
            //     valid = false;
            // }else {
            //     valid = true;
            // }
            if(!user[each?.key] || user[each?.key] === ''){
                _errObject[each?.key] = each?.label+' is required';
                valid = false;
            }else {
                _errObject[each?.key] = '';
            }
        });
        form1.map((each) => {
            if(!user[each?.key] || user[each?.key] === ''){
                _errObject[each?.key] = each?.label+' is required';
                valid = false;
            }else {
                _errObject[each?.key] = '';
            }
        });
        /**
         * Custom Validation
         */
        if(user['mobile'].length < 10){
            _errObject['mobile'] = 'Mobile number must me 10 digits';
            valid = false;
        }else {
            _errObject['mobile'] = '';
        }
        setUserError({..._errObject});
        console.log('Valid--> ', valid);
        console.log('Error Object --> ', _errObject);

        return valid;
    }
    const handleUpdateUserData = () => {
        /**
         * Validation for all the fields
         */
        if(validateFields()){
            /**
             * Add Api call to update the user
             */
            axios.patch(
                'https://api.gssgrcsuite.com/v1/users/'+user.id,{
                    firstName: user.firstName,
                    lastName: user.lastName,
                    mobile: user.mobile,
                    contactNo: user.contactNo,
                },{
                    headers: {
                        Authorization: 'Bearer '+localStorage.getItem('test-token')
                    }
                }
            )
                .then((response) => {
                    const {data} = response;
                    /**
                     * Do post login things here
                     */
                    setEdit(false);
                    enqueueSnackbar('User profile updated successfully', {
                        variant: 'success',
                    });
                    localStorage.setItem('test-user', JSON.stringify(data));
                })
                .catch((error) => {
                    const data = error?.response?.data
                    enqueueSnackbar(data?.message || 'Something went wrong', {
                        variant: 'error',
                        anchorOrigin: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                })
                .finally(() => {
                    setLoading(false);
                })
        }
    }

    return(
        <Stack>
            <Box display={'flex'}>
                {
                    !edit ?
                        <Button onClick={() => {
                            setEdit(true);
                        }}>
                            Edit
                        </Button>
                        :
                        <Box display={'flex'}>
                            <Button onClick={() => {
                                setEdit(false);
                                setUser(
                                    JSON.parse(localStorage.getItem('test-user'))
                                );
                            }}>
                                Cancel
                            </Button>
                            <Button onClick={handleUpdateUserData}>
                                Save
                            </Button>
                        </Box>
                }
            </Box>
            <Grid container spacing={2}>
                <Grid container item xs={12} sm={6}>
                    {
                        form.map((each) => (
                            <Grid item xs={12}>
                                <EditDataField
                                    helperText={userError[each?.key] || ''}
                                    label={each.label} //object.keyName object[keyName]
                                    value={user ? user[each.key] : ''}
                                    edit={edit}
                                    onChange={(e) => {
                                        setUser((user) => {
                                            user[each.key] = e.target.value;
                                            return ({...user});
                                        })
                                    }}
                                />
                            </Grid>

                        ))
                    }
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                {
                    form1.map((each) => (
                        <Grid item xs={12} sm={6}>
                            <EditDataField
                                helperText={userError[each?.key] || ''}
                                label={each.label} //object.keyName object[keyName]
                                value={user ? user[each.key] : ''}
                                edit={edit}
                                onChange={(e) => {
                                    setUser((user) => {
                                        user[each.key] = e.target.value;
                                        return ({...user});
                                    })
                                }}
                            />
                        </Grid>

                    ))
                }
            </Grid>
            <Grid maxWidth={'sm'} sx={{mt: 4}}>
                <ReadMoreText value={bigString} />
            </Grid>


        </Stack>
    )

}