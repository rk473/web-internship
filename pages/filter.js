import {useEffect, useState} from "react";
import {Box, Checkbox, Grid, Paper, Slider, Stack, Typography} from "@mui/material";

const filterData = [
    {label: '1 star', value: 1},
    {label: '2 star', value: 2},
    {label: '3 star', value: 3},
    {label: '4 star', value: 4},
    {label: '5 star', value: 5},
]

const genderData = [
    {label: 'Male', value: 1},
    {label: 'Female', value: 2},
]

const fixData = [
    {name: 'Demo', star: 1, gender: 1, age: 21},
    {name: 'Demo2', star: 3, gender: 2, age: 10},
    {name: 'Demo3', star: 2, gender: 1, age: 34},
    {name: 'Demo4', star: 1, gender: 2, age: 50},
    {name: 'Demo5', star: 2, gender: 1, age: 65},
    {name: 'Demo6', star: 1, gender: 1, age: 8},
    {name: 'Demo7', star: 5, gender: 1, age: 52},
    {name: 'Demo8', star: 4, gender: 2, age: 67},
    {name: 'Demo9', star: 1, gender: 1, age: 25},
    {name: 'Demo10', star: 3, gender: 2, age: 22},
]

export default function JsonData() {

    const [filter, setFilter] = useState({
        starFilter: [],
        genderFilter: [],
        ageFilter: [0,70],
    });

    const [data, setData] = useState([]);

    useEffect(() => {
        let _data = fixData;
        if(filter.starFilter.length > 0){
            _data = _data.filter(e => filter.starFilter.indexOf(e.star) > -1);
        }
        if(filter.genderFilter.length > 0){
            _data = _data.filter(e => filter.genderFilter.indexOf(e.gender) > -1);
        }
        if(filter.ageFilter.length > 0){
            _data = _data.filter(e => filter.ageFilter[0] <= e.age && e.age <= filter.ageFilter[1]);
        }
        setData([..._data]);
    }, [filter]);

    const handleChangeFilter = (e, each, filterName) => {
        const checked = e.target.checked;
        if(checked) {
            setFilter((filter) => {
                filter[filterName].push(each.value);
                return {...filter};
            })
        }else {
            setFilter((filter) => {
                filter[filterName] = filter[filterName].filter(e => e !== each.value);
                return {...filter};
            })
        }
    }

    const handleChangeFilterSlider = (event, newValue) => {
        setFilter((filter) => {
            filter['ageFilter'] = newValue;
            return {...filter};
        })
    }


    return(
        <Box display={'flex'}>
            <Stack width={350} height={'92vh'} bgcolor={'#faf8f8'}>
                <Stack>
                    {
                        filterData.map((e, pos) => (
                            <Box display={'flex'} alignItems={'center'}>
                                <Checkbox
                                    checked={filter.starFilter.indexOf(e.value) > -1}
                                    onChange={(event) => handleChangeFilter(event, e, 'starFilter' )}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                                <Typography ml={1}>
                                    {e.label}
                                </Typography>
                            </Box>
                        ))
                    }
                </Stack>
                <Stack mt={2}>
                    {
                        genderData.map((e, pos) => (
                            <Box display={'flex'} alignItems={'center'}>
                                <Checkbox
                                    checked={filter.genderFilter.indexOf(e.value) > -1}
                                    onChange={(event) => handleChangeFilter(event, e, 'genderFilter' )}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                                <Typography ml={1}>
                                    {e.label}
                                </Typography>
                            </Box>
                        ))
                    }
                </Stack>
                <Stack mt={2} px={2}>
                    <Typography mb={1}>
                        Age Filter
                    </Typography>
                    <Slider
                        value={filter.ageFilter}
                        onChange={handleChangeFilterSlider}
                        valueLabelDisplay="auto"
                        min={0}
                        max={70}
                    />
                </Stack>
            </Stack>
            <Stack width={'100%'} height={'92vh'} bgcolor={'#faf8f8'} mt={3}>
                <Grid container spacing={2}>
                    {
                        data.map((each, pos) => (
                            <Grid item xs={6} sm={4} md={3}>
                                <Paper variant={'outlined'} sx={{p: 2}}>
                                    <Typography>
                                        {each?.name}
                                    </Typography>
                                    <Typography>
                                        {genderData.filter(e => e.value === each?.gender)[0]?.label}
                                    </Typography>
                                    <Typography>
                                        {filterData.filter(e => e.value === each?.star)[0]?.label}
                                    </Typography>
                                    <Typography>
                                        {each?.age}
                                    </Typography>
                                </Paper>
                            </Grid>
                        ))
                    }
                </Grid>
            </Stack>
        </Box>
    )
}
